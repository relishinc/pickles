import FocusTrap from '../utils/focus-trap';
import { whichTransitionEvent } from '../utils/utils';
import '../polyfills/custom-event';

/* Lightbox plugin
----------------------------- */

export default class Lightbox {

    constructor($options = {}) {
        // settings

        let defaults = {
            bodyOpenClass: 'lightbox--open',
            selector: '[data-lightbox]',
            closeButton: false
        };
        this.options = Object.assign({}, defaults, $options);

        // vars

        this.namespace = 'lightbox';
        this.lightboxes = $(this.options.selector);
        this.current = null;
        this.lightboxElement = null;
        this.focusTrap = new FocusTrap();

        // go

        this.init();
    }

    init() {
        this.lightboxes
            .off(`click.${this.namespace}`)
            .on(`click.${this.namespace}`, (e) => {
                e.preventDefault();
                this.createLightbox(e);
            });
    }

    getNextLightbox() {
        let 
            index = this.current;

        if (index < this.lightboxes.length - 1) {
            index++;
            return this.lightboxes.get(index);
        }
        return false;
    };

    getPrevLightbox() {
        let 
            index = this.current;

        if (index > 0) {
            index--;
            return this.lightboxes.get(index);
        }
        return false;
    }

    replaceLightbox(e) {
        if ( e.currentTarget ) this.setImage(e);
    }

    setImage(e) {
        let
            img = $('<img />'),
            src = e.currentTarget.href,
            imgEl = this.lightboxElement.find('.lightbox__image'),
            captionEl = this.lightboxElement.find('.lightbox__caption'),
            nextBtn = this.lightboxElement.find('.lightbox__nav .nav--next'),
            prevBtn = this.lightboxElement.find('.lightbox__nav .nav--prev') ;

        this.lightboxElement.removeClass('lightbox--loaded');

        // set next/prev targets and nav visibility

        this.current = this.lightboxes.index(e.currentTarget);
        this.$next = this.getNextLightbox();
        this.$prev = this.getPrevLightbox();

        nextBtn.toggle(!!this.$next);
        prevBtn.toggle(!!this.$prev);

        // focus trap
        
        this.focusTrap.start(this.lightboxElement);

        // listen for load event

        img
            .attr('src', src)
            .on('load', (e) => {
                this.lightboxElement
                    .trigger(`loaded.${this.namespace}`)
                    .addClass('lightbox--loaded');
            });

        // set background image   

        imgEl
            .css('backgroundImage', `url(${src})`); 

        // set caption

        captionEl
            .text($(e.currentTarget).attr('alt') || $(e.currentTarget).attr('title') || '');

        // show it

        if (!$('body').hasClass('lightbox--open')) {
            setTimeout(() => {
                this.show();
            }, 50);
        }

    }

    createLightbox(e) {
        let
            navEl = $('<div class="lightbox__nav" />'),
            imgEl = $('<div class="lightbox__image" />'),
            captionEl = $('<div class="lightbox__caption" />'),

            nextBtn = $('<a href="#" class="nav nav--next">Next image</a>'),
            prevBtn = $('<a href="#" class="nav nav--prev">Previous image</a>'),
            closeBtn = $('<a href="#" class="nav nav--close">Close</a>');

        // create the root element

        this.lightboxElement = $('<div class="lightbox" />');

        // append to body    

        this.lightboxElement
            .prependTo($('body'))
            .append(imgEl);
        
        // add caption

        captionEl
            .appendTo(this.lightboxElement);

        // add nav

        navEl
            .prependTo(this.lightboxElement);

        nextBtn
            .on(`click.${this.namespace}`, e => {
                e.preventDefault();
                e.stopPropagation();
                this.replaceLightbox({ currentTarget: this.$next });
            })
            .appendTo(navEl);

        prevBtn
            .on(`click.${this.namespace}`, e => {
                e.preventDefault();
                e.stopPropagation();
                this.replaceLightbox({ currentTarget: this.$prev });
            })
            .appendTo(navEl);

        if (this.options.closeButton) {
            closeBtn
                .on(`click.${this.namespace}`, e => {
                    e.preventDefault();
                    this.hide();
                })
                .appendTo(navEl);
        }

        // click handler

        this.lightboxElement
            .on(`click.${this.namespace}`, (e) => {
                this.hide();
            });

        // set image

        this.setImage(e);
    }

    show() {
        this.lightboxElement
            .trigger(`show.${this.namespace}`)
            .addClass('lightbox--open');

        $('body')
            .addClass(this.options.bodyOpenClass);

        // key listener

        $(document)
            .off(`keydown.${this.namespace}`)
            .on(`keydown.${this.namespace}`, e => this.keyHandler(e));   

        // fire event

        document
            .dispatchEvent(new CustomEvent(`${this.namespace}Open`, {
                detail: {
                    el: this.lightboxElement
                }
            }));
    }

    hide() {
        this.lightboxElement
            .one(`${whichTransitionEvent()}`, e => {
                this.destroy();
            })
            .trigger(`hide.${this.namespace}`)
            .removeClass('lightbox--open');

        $('body')
            .removeClass(this.options.bodyOpenClass);

        // key listener

        $(document)
            .off(`keydown.${this.namespace}`);

        // fire event

        document
            .dispatchEvent(new CustomEvent(`${this.namespace}Close`, {
                detail: {
                }
            }));

        // focus trap

        this.focusTrap.stop();
    }

    destroy() {
        this.lightboxElement.remove();
    }


    // handle key events

    keyHandler(e) {
        const KEY_ESC = 27;
        const KEY_RIGHT = 39;
        const KEY_LEFT = 37;

        // which keys are pressed

        switch (e.keyCode) {
            case KEY_ESC:
                this.hide();
                break;
            case KEY_RIGHT:
                this.lightboxElement.find('.nav--next').trigger(`click.${this.namespace}`);
                break;                
            case KEY_LEFT:
                this.lightboxElement.find('.nav--prev').trigger(`click.${this.namespace}`);
                break;            
            default:
                break;
        }

    }     

}